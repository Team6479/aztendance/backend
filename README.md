# Backend

A Go server for AZtendance, our team's attendance tracking system

## Endpoints
Unless otherwise noted, all endpoints expect a POST request
with all fields encoded as form data.
Timestamps are in seconds and use UTC.

### /api/auth/login
Expects `usr` and `pwd`.

Example response:
```json
{
  "sessionInfo": {
    "user": "bob",
    "start": 1623229448,
    "expires": 1623341031
  },
  "sessionID": "c1c8c307aef67b2d687e49d3b24ec478"
}
```

### /api/auth/logout
Expects `sess`.
No meaningful response should be expected.

### /api/auth/check
Expects `sess`.
Returns the session info (same format as `/api/auth/login`) if the session is valid,
or an error message with status 403 if not.

### /api/meeting/create
Expects `sess` and `loc`. User must have sudo.

`loc` is a string containing the location of the meeting.
There are no preset values, but implementations should
use a consistent naming scheme.
Preferably, values should be selected from a dropdown
(i.e. not user input) when possible.

Example response:
```json
{
  "meetingID": "8dedac",
  "meetingCode": "f790",
  "start": 1623229448,
  "loc": "cds"
}
```

### /api/meeting/end
Expects `sess` and `id`. User must have sudo.
No meaningful response should be expected.

### /api/meeting/get
Expects `sess` and `id`. User must have sudo.
Returns information about a meeting
in the same format as `/api/meeting/create`.

### /api/meeting/join
Expects `sess`, `id`, and `code`.
Returns an error if the code is not valid.
Note that the user is implied by session,
and the timestamp is implied by the time of the request.
No further response should be expected.

### /api/meeting/leave
Expects `sess` and `id`. User must have sudo.
Returns an error if the user is not currently in the meeting.
No further response should be expected.

### /api/user/create
Expects `sess`, `usr`, `pwd`, and `sudo`. User must have sudo.
`usr` and `pwd` are the username and password (unhashed) of the new user.
`sudo` should be one of `0` or `1`, where `1` grants administrative privileges.
Currently, the username cannot be changed.

Example response (note that the hash is not included):
```json
{
  "usr": "bob",
  "name": "Bob Doe",
  "sudo": false
}
```

### /api/user/get
Expects `sess` and `usr`. User must have sudo,
unless `usr` matches the user on `sess`
(i.e. the user is requesting info about themself).
Provides a response in the same format as `/api/user/create`.

### /api/user/chsudo
Expects `sess`, `usr`, and `sudo`. User must have sudo.
`sudo` should be one of `0` or `1`, where `1` grants administrative privileges.
No meaningful response should be expected.

### /api/user/passwd
Expects `sess`, `usr`, and `pwd`. User must have sudo,
unless changing their own password. `pwd` is unhashed.
No meaningful response should be expected.

### /api/user/rename
Expects `sess`, `usr`, and `name`.
User must have sudo, unless renaming themself.
This changes the user's real name (e.g. "Bob Doe"), not their username (e.g. "bob")
No meaningful response should be expected.

### /api/user/list
Expects `sess`. User must have sudo.
Returns an array of every user,
with each being in the same form as `/api/user/create`.

Example response:
```json
[
  {
    "usr": "bob",
    "name": "Bob Doe",
    "sudo": false
  },
  {
    "usr": "alice",
    "name": "Alice Roe",
    "sudo": true
  },
  ...
]
```

### /api/user/log
Expects `sess` and `usr`. User must have sudo,
unless the user is requesting a log of their own activity.

Example response:
```json
[
  {
    "id": "",
    "loc": "cds",
    "joined": 0,
    "left": 1
  },
  ...
]
```
