package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type SemVer struct {
	Major uint8 `json:"major"`
	Minor uint8 `json:"minor"`
	Patch uint8 `json:"patch"`
}

var Version = SemVer{1, 0, 0}

func versionHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("[Request] %s", r.URL.Path)
	data, err := json.Marshal(Version)
	if err == nil {
		fmt.Fprintf(w, "%s", data)
	} else {
		log.Printf("[Error] %s", err)
	}
}
