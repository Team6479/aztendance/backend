package util

import "time"

func GetUTCTimestampS() int {
	// WARNING: Casting int64 to int32
	// will cause this to break in 2038e
	// In the unlikelt event that we're
	// still using this at that point,
	// everything will need to be upgraded
	// to int64, including the PostgreSQL db.
	return int(time.Now().UTC().Unix())
}
