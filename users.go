package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"aztendance.team6479.org/backend/db"
)

func userCreateHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("[Request] %s", r.URL.Path)
	sess, err := authenticate(r)
	if err == nil {
		suser, serr := db.GetUser(sess.SessionInfo.User)
		if serr != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintln(w, "Error getting logged in user")
		} else if !suser.Sudo {
			w.WriteHeader(http.StatusForbidden)
			fmt.Fprintln(w, "Must be sudo to create user")
		}
		usr, usrOk := r.Form["usr"]
		name, nameOk := r.Form["name"]
		pwd, pwdOk := r.Form["pwd"]
		sudo, sudoOk := r.Form["sudo"]
		if usrOk && nameOk && pwdOk && sudoOk {
			user, err := db.CreateUser(usr[0], name[0], pwd[0], sudo[0] == "1")
			if err == nil {
				data, jsonErr := json.Marshal(user)
				if jsonErr == nil {
					fmt.Fprintf(w, "%s", data)
				} else {
					log.Printf("[Error] %s", err)
					w.WriteHeader(http.StatusInternalServerError)
					fmt.Fprintf(w, "Could not marshal json: %s", jsonErr)
				}
			} else {
				// TODO: better status codes
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprintf(w, "Could not create user: %s", err)
			}
		} else {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(w, "Requisite fields not submitted")
		}
	} else {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "%s", err)
	}
}

func userGetHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("[Request] %s", r.URL.Path)
	sess, err := authenticate(r)
	if err == nil {
		// TODO: abstract into method for checking self or sudo
		user, uerr := db.GetUser(sess.SessionInfo.User)
		if sess.SessionInfo.User == r.Form["usr"][0] || (uerr == nil && user.Sudo) {
			ruser, ruerr := db.GetUser(r.Form["usr"][0])
			if ruerr == nil {
				json, jerr := json.Marshal(ruser)
				if jerr == nil {
					fmt.Fprintf(w, "%s", json)
				} else {
					w.WriteHeader(http.StatusInternalServerError)
					fmt.Fprintf(w, "%s", jerr)
				}
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprintf(w, "%s", ruerr)
			}
		} else {
			w.WriteHeader(http.StatusForbidden)
			fmt.Fprintf(w, "%s", uerr)
		}
	} else {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "%s", err)
	}
}

func userLogHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("[Request] %s", r.URL.Path)
	sess, err := authenticate(r)
	if err == nil {
		user, uerr := db.GetUser(sess.SessionInfo.User)
		if sess.SessionInfo.User == r.Form["usr"][0] || (uerr == nil && user.Sudo) {
			log, lerr := db.GetUserLog(r.Form["usr"][0])
			if lerr == nil {
				json, jerr := json.Marshal(log)
				if jerr == nil {
					fmt.Fprintf(w, "%s", json)
				} else {
					w.WriteHeader(http.StatusInternalServerError)
					fmt.Fprintf(w, "%s", jerr)
				}
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprintf(w, "%s", lerr)
			}
		} else {
			w.WriteHeader(http.StatusForbidden)
			fmt.Fprintf(w, "%s", uerr)
		}
	} else {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "%s", err)
	}
}

func userSudoHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("[Request] %s", r.URL.Path)
	sess, err := authenticate(r)
	if err == nil {
		user, uerr := db.GetUser(sess.SessionInfo.User)
		if uerr == nil && user.Sudo {
			_, ruerr := db.ChsudoUser(r.Form["usr"][0], r.Form["sudo"][0] == "1")
			if ruerr == nil {
				// do nothing
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprintf(w, "%s", ruerr)
			}
		} else {
			w.WriteHeader(http.StatusForbidden)
			fmt.Fprintf(w, "%s", uerr)
		}
	} else {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "%s", err)
	}
}

func userRenameHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("[Request] %s", r.URL.Path)
	sess, err := authenticate(r)
	if err == nil {
		user, uerr := db.GetUser(sess.SessionInfo.User)
		if sess.SessionInfo.User == r.Form["usr"][0] || (uerr == nil && user.Sudo) {
			_, ruerr := db.RenameUser(r.Form["usr"][0], r.Form["name"][0])
			if ruerr == nil {
				// do nothing
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprintf(w, "%s", ruerr)
			}
		} else {
			w.WriteHeader(http.StatusForbidden)
			fmt.Fprintf(w, "%s", uerr)
		}
	} else {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "%s", err)
	}
}

func userPasswdHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("[Request] %s", r.URL.Path)
	sess, err := authenticate(r)
	if err == nil {
		user, uerr := db.GetUser(sess.SessionInfo.User)
		if sess.SessionInfo.User == r.Form["usr"][0] || (uerr == nil && user.Sudo) {
			_, ruerr := db.ChpasswdUser(r.Form["usr"][0], r.Form["pwd"][0])
			if ruerr == nil {
				// do nothing
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprintf(w, "%s", ruerr)
			}
		} else {
			w.WriteHeader(http.StatusForbidden)
			fmt.Fprintf(w, "%s", uerr)
		}
	} else {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "%s", err)
	}
}

func userListHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("[Request] %s", r.URL.Path)
	sess, err := authenticate(r)
	if err == nil {
		user, uerr := db.GetUser(sess.SessionInfo.User)
		if uerr == nil && user.Sudo {
			users := db.ListUsers()
			data, jsonErr := json.Marshal(users)
			if jsonErr == nil {
				fmt.Fprintf(w, "%s", data)
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprintf(w, "Could not marshal json: %s", jsonErr)
			}
		} else {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(w, "Requisite fields not submitted")
		}
	} else {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "%s", err)
	}
}
