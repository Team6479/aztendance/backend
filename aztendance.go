package main

import (
	"log"
	"net/http"

	"aztendance.team6479.org/backend/db"
)

func main() {
	db.PostgresInit()

	http.HandleFunc("/api/version", versionHandler)
	http.HandleFunc("/api/auth/login", loginHandler)
	http.HandleFunc("/api/auth/logout", logoutHandler)
	http.HandleFunc("/api/auth/check", checkHandler)
	http.HandleFunc("/api/meeting/create", meetingCreateHandler)
	http.HandleFunc("/api/meeting/end", meetingEndHandler)
	http.HandleFunc("/api/meeting/get", meetingGetHandler)
	http.HandleFunc("/api/meeting/join", meetingJoinHandler)
	http.HandleFunc("/api/meeting/leave", meetingLeaveHandler)
	http.HandleFunc("/api/state/get", stateGetHandler)
	http.HandleFunc("/api/user/create", userCreateHandler)
	http.HandleFunc("/api/user/get", userGetHandler)
	http.HandleFunc("/api/user/chsudo", userSudoHandler)
	http.HandleFunc("/api/user/passwd", userPasswdHandler)
	http.HandleFunc("/api/user/rename", userRenameHandler)
	http.HandleFunc("/api/user/list", userListHandler)
	http.HandleFunc("/api/user/log", userLogHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
