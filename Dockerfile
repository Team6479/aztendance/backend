FROM golang:1.16
WORKDIR /root/backend
RUN go get -d -v golang.org/x/net/html
COPY . ./
ENV CGO_ENABLED=0
RUN go build -o backend .

FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /root/backend ./
ENTRYPOINT [ "./backend" ]