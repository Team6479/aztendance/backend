package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"aztendance.team6479.org/backend/db"
)

func meetingGetHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("[Request] %s", r.URL.Path)
	sess, err := authenticate(r)
	if err == nil {
		user, uerr := db.GetUser(sess.SessionInfo.User)
		code, codeOk := r.Form["code"]
		meeting, merr := db.GetMeeting(r.Form["id"][0])
		if merr == nil {
			if uerr == nil && (user.Sudo || (codeOk && code[0] == meeting.MeetingCode)) {
				json, jerr := json.Marshal(meeting)
				if jerr == nil {
					fmt.Fprintf(w, "%s", json)
				} else {
					w.WriteHeader(http.StatusInternalServerError)
					fmt.Fprintf(w, "%s", err)
				}
			} else {
				w.WriteHeader(http.StatusForbidden)
				fmt.Fprintf(w, "%s", err)
			}
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "%s", err)
		}
	} else {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "%s", err)
	}
}

func meetingCreateHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("[Request] %s", r.URL.Path)
	sess, err := authenticate(r)
	if err == nil {
		user, uerr := db.GetUser(sess.SessionInfo.User)
		if uerr == nil && user.Sudo {
			meeting, merr := db.CreateMeeting(r.Form["loc"][0])
			if merr == nil {
				json, jerr := json.Marshal(meeting)
				if jerr == nil {
					fmt.Fprintf(w, "%s", json)
				} else {
					w.WriteHeader(http.StatusInternalServerError)
					fmt.Fprintf(w, "%s", jerr)
				}
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprintf(w, "%s", merr)
			}
		} else {
			w.WriteHeader(http.StatusForbidden)
			fmt.Fprintf(w, "%s", uerr)
		}
	} else {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "%s", err)
	}
}

func meetingEndHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("[Request] %s", r.URL.Path)
	sess, err := authenticate(r)
	if err == nil {
		user, uerr := db.GetUser(sess.SessionInfo.User)
		if uerr == nil && user.Sudo {
			merr := db.EndMeeting(r.Form["id"][0])
			if merr == nil {
				// do nothing
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprintf(w, "%s", merr)
			}
		} else {
			w.WriteHeader(http.StatusForbidden)
			fmt.Fprintf(w, "%s", uerr)
		}
	} else {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "%s", err)
	}
}

func meetingJoinHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("[Request] %s", r.URL.Path)
	sess, err := authenticate(r)
	if err == nil {
		user, uerr := db.GetUser(sess.SessionInfo.User)
		if uerr == nil {
			meeting, merr := db.GetMeeting(r.Form["id"][0])
			if merr == nil {
				if meeting.MeetingCode == r.Form["code"][0] {
					jerr := db.JoinMeeting(meeting.MeetingID, user.User)
					if jerr == nil {
						// do nothing
					} else {
						w.WriteHeader(http.StatusInternalServerError)
						fmt.Fprintf(w, "%s", jerr)
					}
				} else {
					w.WriteHeader(http.StatusForbidden)
					fmt.Fprintln(w, "Invalid meeting code")
				}
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprintf(w, "%s", merr)
			}
		} else {
			w.WriteHeader(http.StatusForbidden)
			fmt.Fprintf(w, "%s", uerr)
		}
	} else {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "%s", err)
	}
}

func meetingLeaveHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("[Request] %s", r.URL.Path)
	sess, err := authenticate(r)
	if err == nil {
		user, uerr := db.GetUser(sess.SessionInfo.User)
		if uerr == nil {
			merr := db.LeaveMeeting(r.Form["id"][0], user.User)
			if merr == nil {
				// do nothing
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprintf(w, "%s", merr)
			}
		} else {
			w.WriteHeader(http.StatusForbidden)
			fmt.Fprintf(w, "%s", uerr)
		}
	} else {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "%s", err)
	}
}

func stateGetHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("[Request] %s", r.URL.Path)
	sess, err := authenticate(r)
	if err == nil {
		user, uerr := db.GetUser(sess.SessionInfo.User)
		if sess.SessionInfo.User == r.Form["usr"][0] || (uerr == nil && user.Sudo) {
			ruser, ruerr := db.GetState(r.Form["usr"][0])
			if ruerr == nil {
				json, jerr := json.Marshal(ruser)
				if jerr == nil {
					fmt.Fprintf(w, "%s", json)
				} else {
					w.WriteHeader(http.StatusInternalServerError)
					fmt.Fprintf(w, "%s", jerr)
				}
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprintf(w, "%s", ruerr)
			}
		} else {
			w.WriteHeader(http.StatusForbidden)
			fmt.Fprintf(w, "%s", uerr)
		}
	} else {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "%s", err)
	}
}
