package main

import (
	"crypto/sha512"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"aztendance.team6479.org/backend/db"
	"aztendance.team6479.org/backend/util"
)

const sessionDuration = 604800 // 1 week in seconds

type SessionResponse struct {
	SessionInfo db.SessionInfo `json:"sessionInfo"`
	SessionID   string         `json:"sessionID"`
}

func authenticate(r *http.Request) (SessionResponse, error) {
	// TODO: authorization header, cookie, JWT?
	perr := r.ParseForm()
	sess, sessOk := r.Form["sess"]
	if perr == nil && sessOk {
		sessInfo, err := db.GetSession(sess[0])
		if err == nil {
			// check expiration
			if sessInfo.Expires >= util.GetUTCTimestampS() {
				return SessionResponse{sessInfo, sess[0]}, nil
			} else {
				// prune expired session
				// TODO: periodically scan for and prune expired sessions
				db.DeleteSession(sess[0])
				return SessionResponse{}, errors.New("Expired session")
			}
		} else {
			return SessionResponse{}, errors.New("Invalid session")
		}
	} else {
		return SessionResponse{}, errors.New("No authentication method presented")
	}
}

/* FIXME: this could cause unexpected behaviour for requests such as
 * /api/auth/login with form data usr=bob&usr=alice&pwd=test
 * better to error when multiple of the same name are submitted */
func loginHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("[Request] %s", r.URL.Path)
	if r.ParseForm() == nil {
		usr, usrOk := r.Form["usr"] // username
		pwd, pwdOk := r.Form["pwd"] // password
		if usrOk && pwdOk {         // check that required fields were submitted
			hasher := sha512.New()
			hasher.Write([]byte(pwd[0])) // hash password with sha512
			hash := fmt.Sprintf("%x", hasher.Sum(nil))
			dbUsr, dbErr := db.GetUser(usr[0])
			if dbErr == nil {
				if hash == dbUsr.Hash {
					timestamp := util.GetUTCTimestampS()
					sessInfo := db.SessionInfo{User: usr[0], Start: timestamp, Expires: timestamp + sessionDuration}
					if exp, expOk := r.Form["exp"]; expOk {
						parsedExp, err := strconv.ParseInt(exp[0], 10, 32) // base10, int32
						if err == nil {
							sessInfo.Expires = int(parsedExp)
						} else {
							// TODO: consider returning an HTTP error here instead of proceeding
							log.Printf("[Error] Couldn't parse exp as int: %s", exp[0])
						}
					}
					sessID, sessErr := db.CreateSession(sessInfo)
					if sessErr == nil {
						data, err := json.Marshal(SessionResponse{sessInfo, sessID})
						if err == nil {
							fmt.Fprintf(w, "%s", data)
						} else {
							w.WriteHeader(http.StatusInternalServerError)
							log.Printf("[Error] Couldn't marshal json: %s", err)
						}
					} else {
						w.WriteHeader(http.StatusInternalServerError)
						fmt.Fprint(w, "[Error] Couldn't create session")
					}
				} else {
					w.WriteHeader(http.StatusForbidden)
					fmt.Fprint(w, "Incorrect username or password")
					log.Printf("[Info] Incorrect login attempt for %s", usr[0])
				}
			} else {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprint(w, "[Error] Couldn't get user")
			}
		} else {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(w, "Requisite fields not submitted")
		}
	} else {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, "Invalid request")
	}
}

func logoutHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("[Request] %s", r.URL.Path)
	sess, err := authenticate(r)
	if err == nil {
		db.DeleteSession(sess.SessionID)
		fmt.Fprintf(w, "%s", sess.SessionID)
	} else {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "%s", err)
	}
}

func checkHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("[Request] %s", r.URL.Path)
	sess, err := authenticate(r)
	if err == nil {
		data, err := json.Marshal(sess)
		if err == nil {
			fmt.Fprintf(w, "%s", data)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			log.Printf("[Error] Couldn't marshal json: %s", err)
		}
	} else {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(w, "%s", err)
	}
}
