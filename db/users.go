package db

import (
	"crypto/sha512"
	"fmt"
)

const (
	sqlGetUser    = `select ID,NAME,HASH,SUDO from USERS where ID=$1`
	sqlCreateUser = `insert into USERS(ID,NAME,HASH,SUDO) values ($1,$2,$3,$4)`
	sqlChsudoUser = `update USERS set SUDO=$1 where ID=$2`
	sqlRenameUser = `update USERS set NAME=$1 where ID=$2`
	sqlPasswdUser = `update USERS set HASH=$1 where ID=$2`
	sqlListUsers  = `select ID,NAME,HASH,SUDO from USERS`
)

type UserInfo struct {
	User string `json:"usr"`
	Name string `json:"name"`
	Hash string `json:"-"`
	Sudo bool   `json:"sudo"`
}

func GetUser(userID string) (UserInfo, error) {
	row := P.QueryRow(sqlGetUser, userID)
	var id, name, hash string
	var sudo bool
	serr := row.Scan(&id, &name, &hash, &sudo)
	if serr != nil {
		return UserInfo{}, serr
	}
	return UserInfo{id, name, hash, sudo}, nil
}

func CreateUser(user string, name string, pwd string, sudo bool) (UserInfo, error) {
	hasher := sha512.New()
	hasher.Write([]byte(pwd))
	hash := fmt.Sprintf("%x", hasher.Sum(nil))
	_, serr := P.Exec(sqlCreateUser, user, name, hash, sudo)
	if serr != nil {
		return UserInfo{}, serr
	}
	return UserInfo{user, name, hash, sudo}, nil
}

func ChsudoUser(user string, sudo bool) (UserInfo, error) {
	userInfo, _ := GetUser(user)
	userInfo.Sudo = sudo
	_, serr := P.Exec(sqlChsudoUser, sudo, user)
	if serr != nil {
		return UserInfo{}, serr
	}
	return userInfo, nil
}

func ChpasswdUser(user string, pwd string) (UserInfo, error) {
	hasher := sha512.New()
	hasher.Write([]byte(pwd))
	hash := fmt.Sprintf("%x", hasher.Sum(nil))
	userInfo, _ := GetUser(user)
	userInfo.Hash = hash
	_, serr := P.Exec(sqlPasswdUser, hash, user)
	if serr != nil {
		return UserInfo{}, serr
	}
	return userInfo, nil
}

func RenameUser(user string, name string) (UserInfo, error) {
	userInfo, _ := GetUser(user)
	userInfo.Name = name
	_, serr := P.Exec(sqlRenameUser, name, user)
	if serr != nil {
		return UserInfo{}, serr
	}
	return userInfo, nil
}

func ListUsers() []UserInfo {
	list := []UserInfo{}
	rows, serr := P.Query(sqlListUsers)
	if serr != nil {
		return list
	}
	for rows.Next() {
		var id, name, hash string
		var sudo bool
		rerr := rows.Scan(&id, &name, &hash, &sudo)
		if rerr != nil {
			return list
		}
		list = append(list, UserInfo{id, name, hash, sudo})
	}
	return list
}
