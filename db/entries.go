package db

const sqlListEntries = `select MEETINGID,LOCATION,TJOINED,TLEFT from ENTRIES where USERID=$1`

type LogEntry struct {
	MeetingID string `json:"id"`
	Location  string `json:"loc"`
	Joined    int    `json:"joined"`
	Left      int    `json:"left"`
}

func GetUserLog(id string) ([]LogEntry, error) {
	list := []LogEntry{}
	rows, serr := P.Query(sqlListEntries, id)
	if serr != nil {
		return []LogEntry{}, serr
	}
	for rows.Next() {
		var meetingID, location string
		var joined, left int
		rerr := rows.Scan(&meetingID, &location, &joined, &left)
		if rerr != nil {
			return []LogEntry{}, serr
		}
		list = append(list, LogEntry{meetingID, location, joined, left})
	}
	return list, nil
}
