package db

import "aztendance.team6479.org/backend/util"

const (
	sqlGetSession    = `select USERID,START,EXPIRES from SESSIONS where ID=$1`
	sqlDeleteSession = `delete from SESSIONS where ID=$1`
	sqlCreateSession = `insert into SESSIONS(ID,USERID,START,EXPIRES) values ($1,$2,$3,$4)`
)

type SessionInfo struct {
	User    string `json:"user"`
	Start   int    `json:"start"`
	Expires int    `json:"expires"`
}

func CreateSession(sess SessionInfo) (string, error) {
	id, rerr := util.RandHex(16) // 16 bytes = 32 chars
	if rerr != nil {
		return "", nil
	}
	// collision avoidance
	_, cerr := GetSession(id)
	if cerr == nil { // yes, == is correct here, not !=
		return CreateSession(sess)
	}
	_, serr := P.Exec(sqlCreateSession, id, sess.User, sess.Start, sess.Expires)
	return id, serr
}

func GetSession(sessID string) (SessionInfo, error) {
	row := P.QueryRow(sqlGetSession, sessID)
	var user string
	var start, expires int
	serr := row.Scan(&user, &start, &expires)
	if serr != nil {
		return SessionInfo{}, serr
	}
	return SessionInfo{user, start, expires}, nil
}

func DeleteSession(sessID string) error {
	_, serr := P.Exec(sqlDeleteSession, sessID)
	return serr
}
