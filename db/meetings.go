package db

import (
	"errors"

	"aztendance.team6479.org/backend/util"
)

const (
	sqlCreateMeeting = `insert into MEETINGS(ID,CODE,START,LOCATION) values ($1,$2,$3,$4)`
	sqlGetMeeting    = `select CODE,START,LOCATION from MEETINGS where ID=$1`
	sqlDeleteMeeting = `delete from MEETINGS where ID=$1`
	sqlCreateState   = `insert into STATES(USERID,MEETINGID,SINCE) values ($1,$2,$3)`
	sqlGetState      = `select MEETINGID,SINCE from STATES where USERID=$1`
	sqlDeleteState   = `delete from STATES where USERID=$1`
	sqlCreateEntry   = `insert into ENTRIES(USERID,MEETINGID,LOCATION,TJOINED,TLEFT) values ($1,$2,$3,$4,$5)`
	sqlListJoined    = `select USERID from STATES where MEETINGID=$1`
)

type MeetingInfo struct {
	MeetingID   string `json:"meetingID"`
	MeetingCode string `json:"meetingCode"`
	Start       int    `json:"start"`
	Location    string `json:"loc"`
}

type State struct {
	UserID    string `json:"userID"`
	MeetingID string `json:"meetingID"`
	Since     int    `json:"since"`
}

func CreateMeeting(loc string) (MeetingInfo, error) {
	id, idErr := util.RandHex(3)
	if idErr != nil {
		return MeetingInfo{}, idErr
	}
	// collision avoidance
	_, cerr := GetMeeting(id)
	if cerr == nil { // yes, == is correct here, not !=
		return CreateMeeting(loc)
	}
	code, codeErr := util.RandHex(2)
	if codeErr != nil {
		return MeetingInfo{}, codeErr
	}
	timestamp := util.GetUTCTimestampS()
	_, serr := P.Exec(sqlCreateMeeting, id, code, timestamp, loc)
	if serr != nil {
		return MeetingInfo{}, serr
	}
	return MeetingInfo{id, code, timestamp, loc}, nil
}

func EndMeeting(id string) error {
	// leave all users in meeting
	rows, rerr := P.Query(sqlListJoined, id)
	if rerr != nil {
		return rerr
	}
	defer rows.Close()
	for rows.Next() {
		var user string
		serr := rows.Scan(&user)
		if serr != nil {
			return serr
		}
		lerr := LeaveMeeting(id, user)
		if lerr != nil {
			return lerr
		}
	}
	_, derr := P.Exec(sqlDeleteMeeting, id)
	return derr
}

func GetMeeting(id string) (MeetingInfo, error) {
	row := P.QueryRow(sqlGetMeeting, id)
	var code, loc string
	var start int
	err := row.Scan(&code, &start, &loc)
	if err != nil {
		return MeetingInfo{}, err
	}
	return MeetingInfo{id, code, start, loc}, nil
}

func GetState(userID string) (State, error) {
	row := P.QueryRow(sqlGetState, userID)
	var meetingID string
	var since int
	serr := row.Scan(&meetingID, &since)
	if serr != nil {
		return State{}, serr
	}
	return State{userID, meetingID, since}, nil
}

func JoinMeeting(meetingID, userID string) error {
	// check that user is not already in meeting
	state, stateErr := GetState(userID)
	if stateErr == nil { // yes, == is correct here, not !=
		return errors.New("User is already in meeting: " + state.MeetingID)
	}
	timestamp := util.GetUTCTimestampS()
	_, err := P.Exec(sqlCreateState, userID, meetingID, timestamp)
	return err
}

func LeaveMeeting(meetingID, userID string) error {
	state, stateErr := GetState(userID)
	if stateErr != nil {
		return stateErr
	}
	meeting, meetingErr := GetMeeting(state.MeetingID)
	if meetingErr != nil {
		return meetingErr
	}
	timestamp := util.GetUTCTimestampS()
	_, eerr := P.Exec(sqlCreateEntry, userID, meetingID, meeting.Location, state.Since, timestamp)
	if eerr != nil {
		return eerr
	}
	_, serr := P.Exec(sqlDeleteState, userID)
	return serr
}
