package db

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/lib/pq"
)

var P *sql.DB

func PostgresInit() {
	psqlconn := fmt.Sprintf("host=postgres-service.aztendance.svc.cluster.local port=5432 user=aztech password=%s dbname=aztendancedb sslmode=disable", os.Getenv("POSTGRES_PASSWORD"))
	var serr error
	P, serr = sql.Open("postgres", psqlconn)
	if serr != nil {
		log.Fatal("Error connecting to PostgreSQL")
	}
	perr := P.Ping()
	if perr != nil {
		fmt.Println(perr)
		log.Fatal("Error pinging PostgreSQL")
	}
	fmt.Println("Connected to PostgreSQL")
}
